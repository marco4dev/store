<?php
namespace App\Models;

class Dvd extends Product
{
    protected $size;

    public function __construct(
        $sku = null,
        $name = null,
        $price = null,
        $size = null,
    )
    {
        parent::__construct($sku, $name, $price);
        $this->size = $size;
    }

    protected function validateSize(): void
    {
        switch (false) {
            case isset($this->size):
                $this->_errors['size'] = 'Size is required';
                break;
            
            case $this->isInt($this->size):
                $this->_errors['size'] = 'Size must be an integer number';
                break;
        }
    }

    protected function validate(): void
    {
        parent::validate();
        $this->validateSize();
    }
}
