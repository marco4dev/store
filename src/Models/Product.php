<?php
namespace App\Models;

use Exception;
use App\Database\Query;

abstract class Product
{
    public const TABLE = "products";

    protected $_id;

    protected $_errors;

    protected $sku;

    protected $name;

    protected $price;
    
    public function __construct(
        $sku = null,
        $name = null,
        $price = null
    )
    {
        $this->_id = null;
        $this->_errors = [];
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
    }
  
    public function __get($prop)
    {
        if(property_exists($this, $prop) && !str_starts_with($prop, '_')) {
            return $this->$prop;
        }
    }

    public function __set($prop, $value)
    {
        if(property_exists($this, $prop) && !str_starts_with($prop, '_')) {
            if(is_string($value)) $value = trim(htmlspecialchars($value));
            $this->$prop = $value;
        }
    }

    protected function prepareData(): array
    {
        $data = [];

        foreach ($this as $prop => $value) {
            if(isset($this->$prop) && !str_starts_with($prop, '_')) {
                $data[$prop] = $value;
            }
        }

        return $data;
    }

    protected function isFloat($f): bool
    { 
        return ($f == (string)(float) $f);
    }

    protected function isInt($i): bool
    { 
        return ($i == (string)(int) $i);
    }

    protected function validateSku(): void
    {
        switch (false) {
            case isset($this->sku):
                $this->_errors['sku'] = 'Sku is required';
                break;

                case is_string($this->sku):
                $this->_errors['sku'] = 'Sku must be string';
                break;

            default:
                $query = new Query();
                $sku = $query->from(self::TABLE)
                    ->select()
                    ->where('sku', '=', $this->sku)
                    ->run()
                    ->rowCount();

                if($sku > 0) $this->_errors['sku'] = 'Sku is already exist';
                break;
        }
    }

    protected function validateName(): void
    {
        switch (false) {
            case isset($this->name):
                $this->_errors['name'] = 'Name is required';
                break;
            
            case is_string($this->name):
                $this->_errors['name'] = 'Name must be string';
                break;
        }
    }

    protected function validatePrice(): void
    {
        switch (false) {
            case isset($this->price):
                $this->_errors['price'] = 'Price is required';
                break;

            case $this->isFloat($this->price):
                $this->_errors['price'] = 'Price must be a float number';
                break;
        }
    }

    protected function validate(): void
    {
        $this->validateSku();
        $this->validateName();
        $this->validatePrice();
    }

    public static function find(array $filter = [], array $select = []): array
    {
        $query = new Query();
        $query->from(self::TABLE)->select($select);

        foreach ($filter as $key => $value) {
            $query->andWhere($key, '=', $value);
        }

        return $query->run()->fetchAll();
    }

    public static function findById(string $id, array $select = []): array
    {
        $query = new Query();
        $query->from(self::TABLE)
            ->select($select)
            ->where('id', '=', $id);

        $model = $query->run()->fetch();

        if(!$model) {
            throw new Exception('Product Not Found', 404);
        }

        return $model;
    }

    public static function deleteManyByIds(array $ids): int
    {
        $query = new Query();
        $query->delete(self::TABLE);

        if(!count($ids)) return 0;

        foreach ($ids as $id) {
            $query->orWhere('id', '=', $id);
        }

        return $query->run()->rowCount();
    }

    public function save(): array
    {
        $data = $this->prepareData();

        $query = new Query();
        $query->into(self::TABLE)
            ->insert($data)->run();

        $id = $query->getConnection()->lastInsertId();
        
        return $this->findById($id);
    }

    public function remove(): int
    {

        if(!isset($this->id)) {
            throw new Exception("Invalid Operation");
        }

        $query = new Query();
        $query->delete(self::TABLE)
            ->where("id", "=", $this->id);

        return $query->run()->rowCount();
    }

    public function errors(): array
    {
        return $this->_errors;
    }

    public function isValid(): bool
    {
        $this->validate();
        return count($this->_errors) === 0;
    }
}
