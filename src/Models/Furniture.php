<?php
namespace App\Models;

class Furniture extends Product
{
    protected $width;

    protected $height;

    protected $length;

    public function __construct(
        $sku = null,
        $name = null,
        $price = null,
        $width = null,
        $height = null,
        $length = null,
    )
    {
        parent::__construct($sku, $name, $price);

        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }

    protected function validateHeight(): void
    {
        switch (false) {
            case isset($this->height):
                $this->_errors['height'] = 'Height is required';
                break;
            
            case $this->isFloat($this->height):
                $this->_errors['height'] = 'Height must be a float number';
                break;
        }
    }

    protected function validateWidth(): void
    {
        switch (false) {
            case isset($this->width):
                $this->_errors['width'] = 'Width is required';
                break;
            
            case $this->isFloat($this->width):
                $this->_errors['width'] = 'Width must be a float number';
                break;
        }
    }

    protected function validateLength(): void
    {
        switch (false) {
            case isset($this->length):
                $this->_errors['length'] = 'Length is required';
                break;
            
            case $this->isFloat($this->length):
                $this->_errors['length'] = 'Length must be a float number';
                break;
        }
    }

    protected function validate(): void
    {
        parent::validate();
        $this->validateHeight();
        $this->validateWidth();
        $this->validateLength();
    }
}
