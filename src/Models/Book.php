<?php
namespace App\Models;

class Book extends Product
{
    protected $weight;

    public function __construct(
        $sku = null,
        $name = null,
        $price = null,
        $weight = null,
    )
    {
        parent::__construct($sku, $name, $price);
        $this->weight = $weight;
    }

    protected function validateWeight(): void
    {
        switch (false) {
            case isset($this->weight):
                $this->_errors['weight'] = 'Weight is required';
                break;
            
            case $this->isFloat($this->weight):
                $this->_errors['weight'] = 'Weight must be a float number';
                break;
        }
    }

    protected function validate(): void
    {
        parent::validate();
        $this->validateWeight();
    }
}
