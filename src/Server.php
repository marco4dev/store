<?php
namespace App;

use App\Database\Query;
use App\Http\Response;
use App\Models\Product;
use Exception;

class Server
{
    private static function init(): void
    {
        self::initDatabase();
        Response::header("Access-Control-Allow-Origin", "*");
        
        self::setErrorHandler();
        self::setExceptionHandler();
    }

    private static function initDatabase() {
        $query = new Query();

       try {
            $query->select([1])
                ->from('products')
                ->run();
       } catch (Exception $e) {
            $query = new Query();
            $query->createTable(Product::TABLE)
                ->addColumn('sku', 'VARCHAR(255)')
                ->addColumn('name', 'VARCHAR(255)')
                ->addColumn('price', 'VARCHAR(255)')
                ->addColumn('size', 'VARCHAR(255)', ['NULL'])
                ->addColumn('weight', 'VARCHAR(255)', ['NULL'])
                ->addColumn('width', 'VARCHAR(255)', ['NULL'])
                ->addColumn('height', 'VARCHAR(255)', ['NULL'])
                ->addColumn('length', 'VARCHAR(255)', ['NULL'])
                ->addIndex('UNIQUE', ['sku'], 'unique_sku')
                ->run();
       }
    }

    private static function setErrorHandler(): void
    {
        set_error_handler([__CLASS__, 'errorHandler']);
    }

    private static function setExceptionHandler(): void
    {
        set_exception_handler([__CLASS__, 'exceptionHandler']);
    }

    public static function exceptionHandler($e): void
    {
        $code = $e->getCode() ? $e->getCode() : 500;
        $body = [];
        $body['statusCode'] = $code;
        $body['error'] = $code >= 500 ? "Internal server error" :$e->getMessage();

        Response::status($code);
        Response::json($body);
        die();
    }

    public static function errorHandler(): void
    {
        $code = 500;
        $body = [];
        $body['statusCode'] = $code;
        $body['error'] = "Internal server error";

        Response::status($code);
        Response::json($body);
        die();
    }

    public static function run(): void
    {
        self::init();

        require __DIR__ . '/routes.php';
        
        Router::run();
    }
}
