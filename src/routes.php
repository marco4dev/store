<?php

use App\Http\Request;
use App\Http\Response;
use App\Models\Product;
use App\Router;

Router::get('/products', function() {
    $res['products'] = Product::find();
    return Response::json($res);
});

Router::post('/products', function() {
    $body = Request::body();
    
    try {
        $type = isset($body->type) ? $body->type : null;
        $class = "App\Models\\$type";
        $product = new $class();
    } catch (Error $e) {
        throw new Exception('Invalid Type', 400);
    }

    foreach ($body as $key => $value) {
        $product->$key = $value;
    }

    if($product->isValid()) {
        $res = [];
        $res['product'] = $product->save();
        return Response::json($res);
    }

    throw new Exception(implode(', ', $product->errors()), 400);

});

Router::delete('/products', function() {
    $body = Request::body();

    if(isset($body->ids) && is_array($body->ids)) {
        $deletedItems = Product::deleteManyByIds($body->ids);

        $res['deletedProducts'] = $deletedItems;
        $res['products'] = Product::find();

        return Response::json($res);
    }

    throw new Exception("Invalid Request", 400);
});
