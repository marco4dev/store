<?php
namespace App\Database;

use PDO, PDOStatement, Exception;

class Query
{
    private $connection;
    private $table;
    private $operation;
    private $tableColumns;
    private $tableIndexes;
    private $tableOptions;
    private $selectColumns;
    private $whereQuery;
    private $whereParams;
    private $insertParams;
    private $updateParams;
    private $orderQuery;
    private $limitQuery;

    public function __construct()
    {
        $this->connection = Driver::getConnection();
        $this->operation = null;
        $this->tableColumns = [];
        $this->tableIndexes = [];
        $this->tableOptions = [];
        $this->selectColumns = [];
        $this->whereQuery = [];
        $this->whereParams = [];
        $this->insertParams = [];
        $this->updateParams = [];
        $this->orderQuery = '';
        $this->limitQuery = '';
    }

    private function addWhereQuery(string $type, string $param, string $condition, string $value): void
    {
        $whereParam = ":$param" . "Param";

        while (isset($this->whereParams[$whereParam])) {
            $whereParam .= rand();
        }

        $query = "$param $condition $whereParam";

        if(count($this->whereQuery) > 0) {
            if(strlen($type) === 0) {
                $type = 'AND';
            }

            $query = " $type " . $query;
        }

        $this->whereQuery[] = $query;
        $this->whereParams[$whereParam] = $value;
    }

    private function buildSelectQuery(): string
    {
        $select = "*";

        if(count($this->selectColumns) > 0) {
            $select = implode(', ', $this->selectColumns);
        }

        return "SELECT $select FROM $this->table";
    }

    private function buildWhereQuery(): string
    {
        $query = '';

        if(count($this->whereQuery) > 0) {
            $query = ' WHERE ';
            $query .= implode('', $this->whereQuery);
        }

        return $query;
    }

    private function buildOrderQuery(): string
    {
        $query = '';

        if(strlen($this->orderQuery) > 0) {
            $query = " ORDER BY $this->orderQuery";
        }

        return $query;
    }

    private function buildLimitQuery(): string
    {
        $query = '';

        if(strlen($this->limitQuery) > 0) {
            $query = " LIMIT $this->limitQuery";
        }

        return $query;
    }

    private function buildInsertQuery(): string
    {
        $query = '';
        
        if(count($this->insertParams) > 0) {
            $query = "INSERT INTO " . $this->table;
            $columns = array_keys($this->insertParams);
            $query .= " (" . implode(', ', $columns) . ")";
            $values = [];

            foreach ($columns as $column) {
                $values[] = ":$column";
            }

            $query .= " VALUES (" . implode(', ', $values) . ")";
        }

        return $query;
    }

    private function buildUpdateQuery(): string
    {
        $query = '';

        if(count($this->updateParams) > 0) {
            $query = "UPDATE $this->table";
            $pairs = [];

            foreach ($this->updateParams as $key => $value) {
                $pairs[] = "$key = :$key";
            }

            $query .= " SET " . implode(', ', $pairs);
        }

        return $query;
    }

    private function buildDeleteQuery(): string
    {
        return "DELETE FROM $this->table";
    }

    private function buildColumn(string $name, string $type, array $attributes): string
    {
        if(!in_array('NULL', $attributes) && !in_array('NOT NULL', $attributes)) {
            $attributes[] = "NOT NULL";
        }

       return "$name $type " . implode(' ', $attributes);
    }

    private function buildColumns(): array
    {
        $columns = [];

        if(!isset($this->tableColumns['id'])) {
            $columns[] = "id INT NOT NULL AUTO_INCREMENT";
        }

        foreach ($this->tableColumns as $name => $options) {
            $type = $options['type'];
            $attributes = $options['attributes'];

            $columns[] = $this->buildColumn($name, $type, $attributes);
        }

        return $columns;
    }

    private function buildIndex(string $type, array $columns, string $name): string
    {
        if($type === "PRIMARY") {
            $type = "PRIMARY KEY";
            $name = "";
        }

        return "$type $name (" . implode(', ', $columns) . ")";
    }

    private function buildIndexes(): array
    {
        $indexes = [];
        $primaryFound = false;

        foreach ($this->tableIndexes as $index) {
            if($index['type'] === 'PRIMARY') {
                $primaryFound = true;
                break;
            }
        }

        if(!$primaryFound) {
            $indexes[] = "PRIMARY KEY (id)";
        }

        foreach ($this->tableIndexes as $index) {
            $type = $index['type'];
            $name = $index['name'];
            $columns = $index['columns'];

            $indexes[] = $this->buildIndex($type, $columns, $name);
        }

        return $indexes;
    }

    private function buildOptions(): array
    {
        $options = [];

        if(!isset($this->tableOptions['ENGINE'])) {
            $this->tableOptions['ENGINE'] = 'InnoDB';
        }

        foreach ($this->tableOptions as $key => $value) {
            $options[] = "$key = $value";
        }

        return $options;
    }

    private function buildCreateTableQuery(): string
    {
        $query = "CREATE TABLE $this->table (";
        $query .= implode(', ', $this->buildColumns());

        $query .= ', ';
        $query .= implode(', ', $this->buildIndexes());

        $query .= ') ';
        $query .= implode(', ', $this->buildOptions());

        return $query;
    }

    private function buildQuery(): string
    {
        if(!isset($this->operation)) {
            throw new Exception('Invalid Query', 500);
        }

        $query = '';

        switch ($this->operation) {
            case 'fetch':
                $query = $this->buildSelectQuery() .
                    $this->buildWhereQuery() .
                    $this->buildOrderQuery() .
                    $this->buildLimitQuery();
                break;
            case 'insert':
                $query = $this->buildInsertQuery();
                break;
            case 'update':
                $query = $this->buildUpdateQuery() . $this->buildWhereQuery();
                break;
            case 'delete':
                $query = $this->buildDeleteQuery() . $this->buildWhereQuery();
                break;
            case 'create':
                $query = $this->buildCreateTableQuery();
        }

        $query .= ';';

        return $query;
    }

    private function prepareQuery(string $query): PDOStatement
    {
        $stmt = $this->connection->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $this->bindParams($stmt);
        return $stmt;
    }

    private function bindParams($stmt): void
    {
        foreach ($this->whereParams as $key => $value) {
            $stmt->bindValue($key, $value);
        }

        foreach ($this->insertParams as $key => $value) {
            $stmt->bindValue($key, $value);
        }

        foreach ($this->updateParams as $key => $value) {
            $stmt->bindValue($key, $value);
        }
    }

    public function getConnection(): PDO
    {
        return $this->connection;
    }

    public function createTable(string $table, array $options = []): self
    {
        $this->table = $table;
        $this->tableOptions = $options;
        $this->operation = 'create';
        return $this;
    }

    public function addColumn(string $column, string $type, array $attributes = []): self
    {

        $this->tableColumns[$column] = [
            'type' => $type,
            'attributes' => $attributes,
        ];

        return $this;
    }

    public function addIndex(string $type, array $columns, string $name): self
    {

        $this->tableIndexes[] = [
            'type' => strtoupper($type),
            'name' => strtolower($name),
            'columns' => $columns,

        ];

        return $this;
    }

    public function from(string $table): self
    {
        $this->table = $table;
        $this->operation = 'fetch';
        return $this;
    }

    public function into(string $table): self
    {
        $this->table = $table;
        $this->operation = 'insert';
        return $this;
    }

    public function update(string $table): self
    {
        $this->table = $table;
        $this->operation = 'update';
        return $this;
    }

    public function delete(string $table): self
    {
        $this->table = $table;
        $this->operation = 'delete';
        return $this;
    }

    public function select(array $columns = []): self
    {
        $this->selectColumns = $columns;
        return $this;
    }

    public function where(string $param, string $condition, string $value): self
    {
        $this->addWhereQuery('', $param, $condition, $value);
        return $this;
    }

    public function andWhere(string $param, string $condition, string $value): self
    {
        $this->addWhereQuery('AND', $param, $condition, $value);
        return $this;
    }

    public function orWhere(string $param, string $condition, string $value): self
    {
        $this->addWhereQuery('OR', $param, $condition, $value);
        return $this;
    }

    public function orderBy(string $query): self
    {
        $this->orderQuery = $query;
        return $this;
    }

    public function limit(int $query): self
    {
        $this->limitQuery = $query;
        return $this;
    }

    public function insert(array $data): self
    {
        $this->insertParams += $data;
        return $this;
    }

    public function set(array $data): self
    {
        $this->updateParams += $data;
        return $this;
    }

    public function run()
    {
        try {
            $query = $this->buildQuery();
            $stmt = $this->prepareQuery($query);
            $stmt->execute();
            return $stmt;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }

    public function raw(string $query): PDOStatement
    {
        try {
            $stmt = $this->connection->prepare($query);
            return $stmt;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 500);
        }
    }
}
