<?php
namespace App\Database;

use PDO, PDOException, Exception;

abstract class Driver
{   
    private static $connection = null;
    
    public static function getConnection(): PDO
    {
        if(!isset(self::$connection)) {
            try {
                $dsn = getenv('DB_DRIVER') . ":host=" . getenv('DB_HOST') . ";dbname=" . getenv('DB_NAME');
                self::$connection = new PDO($dsn, getenv('DB_USER'), getenv('DB_PASS'));
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                throw new Exception($e->getMessage(), 500);
            }
        }

        return self::$connection;
    }
}
