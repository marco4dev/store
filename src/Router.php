<?php
namespace App;

use App\Http\Request;
use App\Http\Response;
use Exception;

final class Router
{
    private static array $routes = [];

    private static function addRoute(Route $route): bool
    {
        foreach (self::$routes as $r) {
            if($r->match($route->method, $route->path)) {
                return false;
            }
        }

        self::$routes[] = $route;
        return true;
    }

    private static function findRoute(): mixed
    {
        $url = Request::url();
        $method = Request::method();

        foreach (self::$routes as $route) {
            if($route->match($method, $url)) {
                return $route;
            }
        }

        return null;
    }

    private static function notFoundHandler(): void
    {
        throw new Exception('Resource Not Found', 404);
    }

    private static function add(string $method, string $path, callable $handler): void
    {
        if(!str_starts_with($path, "/")) {
            $path = '/' . $path;
        }

        $route = new Route($method, $path, $handler);
        self::addRoute($route);
    }

    public static function get(string $path, callable $handler): void
    {
        self::add('GET', $path, $handler);
    }

    public static function post(string $path, callable $handler): void
    {
        self::add('POST', $path, $handler);
    }

    public static function patch(string $path, callable $handler): void
    {
        self::add('PATCH', $path, $handler);
    }

    public static function put(string $path, callable $handler): void
    {
        self::add('PUT', $path, $handler);
    }

    public static function delete(string $path, callable $handler): void
    {
        self::add('DELETE', $path, $handler);
    }

    public static function run(): void
    {
        if(Request::method() === 'OPTIONS') {
            Response::header("Access-Control-Allow-Methods", 'POST, PATCH, PUT, DELETE, OPTIONS');
            Response::header("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Methods, Origin");
            Response::status(204);
            return;
        }

        $route = self::findRoute();
        $handler = "self::notFoundHandler";

        if(isset($route)) {
            $handler = $route->handler;
        }

        call_user_func($handler);
    }
}
