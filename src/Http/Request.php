<?php
namespace App\Http;

class Request
{
    private const PREFIX = '';

    public static function query(string $param = ''): mixed
    {
        if(strlen($param) > 0) {
            return isset($_GET[$param]) ? $_GET[$param] : null;
        }

        return $_GET;
    }

    public static function params(string $param = ''): mixed
    {
        if(strlen($param) > 0) {
            return isset($_POST[$param]) ? $_POST[$param] : null;
        }

        return $_POST;
    }

    public static function body(): mixed
    {
        return json_decode(file_get_contents('php://input'));
    }

    public static function method(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public static function url(): string
    {
        $url = explode('?', $_SERVER['REQUEST_URI'])[0];
        $prefixLength = strlen(self::PREFIX);

        if($prefixLength > 0 && str_starts_with($url, self::PREFIX)) {
            $url = substr($url, $prefixLength);
        }

        return $url;
    }

    public static function header(string $name): string
    {
        return getallheaders()[$name];
    }

    public static function headers(): array
    {
        return getallheaders();
    }
}
