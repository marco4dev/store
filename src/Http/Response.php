<?php
namespace App\Http;

class Response
{
    public static function status(int $code): void
    {
        http_response_code($code);
    }

    public static function header(string $name, string $value): void
    {
        header("$name: $value");
    }

    public static function json($body): void
    {
        self::header('Content-Type', 'application/json');
        echo json_encode($body);
    }

    public static function plain($body): void
    {
        self::header('Content-Type', 'text/plain');
        print_r($body);
    }
}
