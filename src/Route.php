<?php
namespace App;

class Route
{
    private string $method;

    private string $path;

    private $handler;

    public function __construct(
        string $method,
        string $path,
        $handler
    )
    {
        $this->method = $method;
        $this->path = $path;
        $this->handler = $handler;
    }

    public function __get($prop)
    {
        if (property_exists($this, $prop)) {
            return $this->$prop;
        }
    }

    public function match(string $method, string $path): bool
    {
        return ($this->path === $path && $this->method === $method);
    }
}
