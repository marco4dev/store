# Database Config

Add these environment variables to .htaccess file:

```
DB_DRIVER=mysql
DB_HOST={host}
DB_NAME={db_name}
DB_USER={username}
DB_PASS={pass}

```

# Router Config

Redirect all requests to index.php from .htaccess file.

May looks like this:

```
<IfModule mod_rewrite.c>
<IfModule mod_negotiation.c>
    Options -MultiViews
</IfModule>

RewriteEngine On

RewriteCond %{REQUEST_FILENAME} -d [OR]
RewriteCond %{REQUEST_FILENAME} -f
RewriteRule ^ ^$1 [N]

RewriteCond %{REQUEST_URI} (\.\w+$) [NC]
RewriteRule ^(.*)$ public/$1

RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php
</IfModule>
```
